﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Olhaminhabola : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        IKTest Ik = other.GetComponent<IKTest>();
        if (Ik)
        {
            Ik.pesoIK = 1;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        IKTest Ik = other.GetComponent<IKTest>();
        if (Ik)
        {
            Ik.pesoIK = 0;
        }
    }
}
