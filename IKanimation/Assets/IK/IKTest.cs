﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKTest : MonoBehaviour {

    Animator anim;
    public Transform olhar;
    public float pesoIK;

    

	void Start () {
        anim = GetComponent<Animator>();
	}
	
	void OnAnimatorIK ()
    {
        anim.SetLookAtWeight(pesoIK);
        anim.SetLookAtPosition(olhar.position);



    }
}
