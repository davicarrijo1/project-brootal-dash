﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class alou : MonoBehaviour
{

    Animator anim;
    public Transform seguirMao;
    public Transform seguirPe;
    public float pesoIK;
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    void OnAnimatorIK()
    {
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, pesoIK);
        anim.SetIKPosition(AvatarIKGoal.LeftHand, seguirMao.position);
        anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, pesoIK);
        anim.SetIKRotation(AvatarIKGoal.LeftHand, seguirMao.rotation);

        anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, pesoIK);
        anim.SetIKPosition(AvatarIKGoal.LeftFoot, seguirPe.position);
        anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, pesoIK);
        anim.SetIKRotation(AvatarIKGoal.LeftFoot, seguirPe.rotation);
    }
}
