﻿public interface ICanvasObject
{
    void InTransition();

    void OutTransition();
}
